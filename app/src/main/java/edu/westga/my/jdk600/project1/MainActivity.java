package edu.westga.my.jdk600.project1;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Stack;

/**
 * This is the only activity in the application.
 * @author David Jarrett, Ras Fincher, Tyler Vega
 * @version 1/20/2018
 */
public class MainActivity extends AppCompatActivity {

    private static final File rootDirectory = Environment.getExternalStorageDirectory();
    private static final String DIRECTORY_HISTORY = "DIR_PERSIST";
    private static final String SORT_ORDER = "SORT_ORDER";

    private enum SortOrder { ASCENDING, DESCENDING }
    private SortOrder sortOrder;

    private ListView fileListView;
    private TextView directoryTitle;
    private List<File> allFiles;
    private List<File> files;
    private List<File> directories;

    private File currentDirectory;
    private Stack<File> directoryHistory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            this.startProgram();
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
        }
    }

    @Override
    public void onRequestPermissionsResult(
            int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            this.startProgram();
        } else {
            System.exit(-1);
        }
    }

    private void startProgram() {
        this.initializeMembers();
        this.initializeListView();
        this.setDirectoryNameText(rootDirectory);
    }

    private void initializeMembers() {
        this.fileListView = findViewById(R.id.file_list_view);
        this.directoryTitle = findViewById(R.id.directory_title);

        this.allFiles = new ArrayList<>();
        this.files = new ArrayList<>();
        this.directories = new ArrayList<>();

        this.currentDirectory = rootDirectory;
        this.directoryHistory = new Stack<>();
    }

    private void initializeListView() {
        this.initializeFiles();
        this.connectAdapter();
        this.connectListItemEventHandler();
    }

    private void initializeFiles() {
        this.clearFileArrays();
        this.loadFileArrays();

        if (this.sortOrder == null || this.sortOrder == SortOrder.ASCENDING) {
            this.sortFileArrays();
        } else {
            this.sortFileArraysDescending();
        }

        this.consolidateFileArrays();
    }

    private void clearFileArrays() {
        this.files.clear();
        this.directories.clear();
    }

    private void loadFileArrays() {
        for (File currentFile : this.currentDirectory.listFiles()) {
            if (currentFile.isDirectory()) {
                directories.add(currentFile);
            } else {
                files.add(currentFile);
            }
        }
    }

    private void sortFileArrays() {
        Collections.sort(files);
        Collections.sort(directories);
        this.sortOrder = SortOrder.ASCENDING;
    }

    private void sortFileArraysDescending() {
        Comparator<File> descendingComparator = new Comparator<File>() {
            @Override
            public int compare(File f1, File f2) {
                return f1.compareTo(f2) * -1;
            }
        };

        Collections.sort(files, descendingComparator);
        Collections.sort(directories, descendingComparator);
        this.sortOrder = SortOrder.DESCENDING;
    }

    private void consolidateFileArrays() {
        this.allFiles.clear();
        this.allFiles.addAll(directories);
        this.allFiles.addAll(files);
    }

    private void connectAdapter() {
        FileAdapter adapter = new FileAdapter(MainActivity.this,
                android.R.layout.simple_list_item_1, this.allFiles, this);
        this.fileListView.setAdapter(adapter);
    }

    private void connectListItemEventHandler() {
        AdapterView.OnItemClickListener listener = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long position) {
                File currentFile = MainActivity.this.allFiles.get((int) position);
                if (currentFile.isDirectory()) {
                    MainActivity.this.loadDirectory(currentFile, true);
                } else {
                    MainActivity.this.handleIntent(currentFile);
                }
            }
        };
        this.fileListView.setOnItemClickListener(listener);
    }

    private void loadDirectory(File directory, boolean saveInStack) {
        if (saveInStack) {
            this.directoryHistory.push(this.currentDirectory);
        }
        this.currentDirectory = directory;
        this.setDirectoryNameText(directory);
        this.initializeFiles();
        this.refreshListView();
    }

    private void setDirectoryNameText(File directory) {
        if (directory == rootDirectory) {
            this.directoryTitle.setText(R.string.root_directory_name);
        } else {
            this.directoryTitle.setText(directory.getName());
        }
    }

    private void refreshListView() {
        ArrayAdapter<File> adapter = (ArrayAdapter<File>) this.fileListView.getAdapter();
        adapter.notifyDataSetChanged();
    }

    private void handleIntent(File currentFile) {
        FileType fileType = FileType.determineFileType(currentFile);
        String mimeType = fileType.getMimeType();

        if (mimeType != null) {
            Uri uri = Uri.fromFile(currentFile);
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(uri, mimeType);
            executeIntent(currentFile, fileType, intent);
        }
    }

    private void executeIntent(File currentFile, FileType fileType, Intent intent) {
        List<ResolveInfo> activities = getPackageManager().queryIntentActivities(intent, 0);

        if (activities.size() > 1) {
            Intent chooser = Intent.createChooser(intent, "Select an app:");
            startActivity(chooser);
        } else if (activities.size() == 1) {
            startActivity(intent);
        } else {
            Toast.makeText(this,
                    "No application exists that can handle this file.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * OnClick handler for the back button. If a directory exists within the directory history
     * stack, then that directory will be popped and loaded. If not, then nothing happens.
     * @preconditions : None
     * @postconditions : User will have moved back one directory level.
     * @param view : Not used.
     */
    public void goBack(View view) {
        if (this.directoryHistory.isEmpty()) {
            return;
        }
        this.loadDirectory(this.directoryHistory.pop(), false);
    }

    /**
     * Saves the current directory to the directory history stack, and writes the stack out to
     * the outState bundle so that the current directory can be restored when the screen is
     * rotated. This also preserves the directory traversal history and sort order.
     * @preconditions : None
     * @postconditions : The current directory will be on top of the directory history stack, and
     *                   this stack will be stored in the outState bundle.
     * @param outState : Where the directory history stack and sort order is stored.
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        this.directoryHistory.push(this.currentDirectory);
        outState.putSerializable(DIRECTORY_HISTORY, this.directoryHistory);
        outState.putSerializable(SORT_ORDER, this.sortOrder);
    }

    /**
     * Restores the directory history stack and the current directory when the device is rotated.
     * Also refreshes the view to the directory that was being displayed when the device rotated,
     * including the sort order.
     * @preconditions : None
     * @postconditions : The directory history stack and the current directory will be restored.
     * @param savedInstanceState : Where the directory history stack is stored.
     */
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        this.directoryHistory = (Stack<File>) savedInstanceState.getSerializable(DIRECTORY_HISTORY);
        this.sortOrder = (SortOrder) savedInstanceState.getSerializable(SORT_ORDER);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!this.directoryHistory.isEmpty()) {
            this.loadDirectory(this.directoryHistory.pop(), false);
        }
    }

    /**
     * Fills the options menu with sorting options.
     * @preconditions : None
     * @postconditions : The options menu will be inflated.
     * @param menu : The menu object to inflate into.
     * @return : Always true
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }

    /**
     * Determines the appropriate sorting method to call based on user input. The directories in
     * the user's view will be sorted appropriately and the view refreshed.
     * @preconditions : None
     * @postconditions : Files will be sorted either ascending or descending.
     * @param item : The menu item that was pressed by the user.
     * @return : Always true
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.sort_ascending:
                this.sortFileArrays();
                break;
            case R.id.sort_descending:
                this.sortFileArraysDescending();
                break;
        }
        this.consolidateFileArrays();
        this.refreshListView();
        return true;
    }

}
