package edu.westga.my.jdk600.project1;

import android.support.annotation.Nullable;

import java.io.File;

/**
 * Created by jdk60 on 2/5/2018.
 */

/**
 * This is where all file-type information for the application is stored. To cause the system to
 * support a new type of file automatically, simply add an entry to the following list using the format:
 *
 * NAME(static_icon_resource, {extension1, extension2, etc.}, mimeType)
 *
 * The FileBrowser application will automatically recognize and act on the new file-type, including
 * setting its icon when encountered in the file-system, as well as launching the correct intent
 * for it when it's tapped on screen.
 */
enum FileType {
    DIRECTORY(R.drawable.ic_directory),
    GENERAL_FILE(R.drawable.ic_file),
    HTML_FILE(R.drawable.ic_html_file, new String[] {"html", "htm"}, "text/html"),
    AUDIO_FILE(R.drawable.ic_audio, new String[] {"mp3", "wav"}, "audio/*"),
    IMAGE_FILE(R.drawable.ic_image_file, new String[] {"jpg", "jpeg", "gif", "png", "bmp"}, "image/*"),
    TEXT_FILE(R.drawable.ic_text_file, new String[] {"txt"}, "text/plain");

    private int imageCode;
    private String[] extensions;
    private String mimeType;

    FileType(int imageCode) {
        this(imageCode, null, null);
    }

    FileType(int imageCode, @Nullable String[] extension, @Nullable String mimeType)
    {
        this.imageCode = imageCode;
        this.extensions = extension;
        this.mimeType = mimeType;
    }

    /**
     * Gets the resource code for the FileTypes image-icon resource.
     * @return The resource code.
     */
    public int getImageCode() {
        return this.imageCode;
    }

    /**
     * Gets the mime type of the FileType.
     * @return The mime type.
     */
    public String getMimeType() {
        return this.mimeType;
    }

    /**
     * Returns an appropriate FileType object that matches the extension of the
     * supplied file.
     * @param currentFile The file you wish you get the FileType for.
     * @return The FileType object matching the supplied file.
     */
    public static FileType determineFileType(File currentFile) {
        if (currentFile.isDirectory()) {
            return DIRECTORY;
        }
        String extension = getExtension(currentFile);
        if (extension == null) {
            return GENERAL_FILE;
        }
        for (FileType currentFileType : FileType.values()) {
            if (currentFileType.hasExtension(extension)) {
                return currentFileType;
            }
        }
        return GENERAL_FILE;
    }

    private static String getExtension(File currentFile) {
        String filename = currentFile.getName();
        int dotIndex = filename.lastIndexOf('.');

        if (dotIndex != -1) {
            return filename.substring(dotIndex + 1, filename.length());
        }
        return null;
    }

    private boolean hasExtension(final String extension) {
        if (this.extensions == null) {
            return false;
        }
        for (String current : this.extensions) {
            if (current.equalsIgnoreCase(extension)) {
                return true;
            }
        }
        return false;
    }

}
