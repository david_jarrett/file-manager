package edu.westga.my.jdk600.project1;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.ThumbnailUtils;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.io.File;
import java.util.List;
import java.util.Objects;

/**
 * This class creates custom views for a ListView. All static icons are loaded within the UI
 * thread. Dynamic image file icons are generated in separate threads from the
 * UI to improve performance. All file-type information is acquired by querying the static
 * methods of the FileType class.
 *
 * @author David Jarrett, Ras Fincher, Tyler Vega
 * @version 01/20/2018
 */
public class FileAdapter extends ArrayAdapter {

    private MainActivity main;
    private Context context;

    public static final int ICON_SIZE = 128;

    /**
     * Initializes a new FileAdapter
     * @param context The activity context.
     * @param resource The resource ID for a layout file containing a TextView to use when instantiating views.
     * @param objects The objects in the ListView.
     * @param main The parent activity of the ListView. This is required to push dynamic image
     *             results back onto the UI thread.
     */
    FileAdapter(@NonNull Context context, int resource, @NonNull List objects, MainActivity main) {
        super(context, resource, objects);
        this.context = context;
        this.main = main;
    }

    /**
     * Creates the new view.
     * @param position Position of the list item associated with the new view.
     * @param convertView
     * @param parent The parent ViewGroup of this view.
     * @return The newly created view.
     */
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater =
                (LayoutInflater) this.context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.custom_for_linearlayout, null);

        TextView filenameTextView = view.findViewById(R.id.filename_text);
        File currentFile = (File) getItem(position);
        this.setText(filenameTextView, currentFile);
        this.setIcon(filenameTextView, currentFile);

        TextView fileSizeTextView = view.findViewById(R.id.file_size);
        this.setFileOrDirectorySizeText(fileSizeTextView, currentFile);

        return view;
    }

    private void setText(TextView filenameTextView, File currentFile) {
        filenameTextView.setText(currentFile.getName());
    }

    private void setIcon(TextView view, File currentFile) {
        FileType fileType = FileType.determineFileType(currentFile);

        if (fileType != FileType.IMAGE_FILE) {
            this.setGenericThumbnail(view, fileType);
        } else {
            DynamicImageGenerator generator =
                    new DynamicImageGenerator(this.context, currentFile, view);
            generator.start();
        }

        view.setCompoundDrawablePadding(10);
    }

    private void setGenericThumbnail(TextView view, FileType fileType) {
        int imageCode = fileType.getImageCode();
        view.setCompoundDrawablesWithIntrinsicBounds(imageCode, 0, 0, 0);
    }

    private void setFileOrDirectorySizeText(TextView view, File file) {
        if(file.isDirectory()) {
            view.setText(this.buildDirectorySize(file));
        } else {
            view.setText(this.buildFileSize(file));
        }
    }

    private String buildDirectorySize(File directory) {
        int size = directory.listFiles().length;
        return String.valueOf(size) + " files";
    }

    private String buildFileSize(File file) {
        final long ONE_MEGABYTE = 1024L * 1024L;
        final long ONE_KILOBYTE = 1024L;
        String result = "";
        long fileSize = file.length();

        if(fileSize >= ONE_MEGABYTE) {
            double size = (double) fileSize / ONE_MEGABYTE;
            result = String.format("%.1f Mb", size);
        } else if (fileSize >= ONE_KILOBYTE) {
            result = (fileSize / ONE_KILOBYTE) + "Kb";
        } else {
            result = fileSize + " bytes";
        }
        return result;
    }

    /*
    This inner class is where the dynamic image icons are generated. This is done in a seperate
    thread from the UI thread, per Android guidelines. The main field of the enclosing class
    is used to access the UI thread when processing is complete so that the newly generated
    image icon can be inserted into the ListView at runtime.
     */
    private class DynamicImageGenerator extends Thread {

        private static final int ICON_SIZE = 128;

        private Context context;
        private File currentFile;
        private TextView view;

        public DynamicImageGenerator(Context context, File currentFile, TextView view) {
            this.context = Objects.requireNonNull(context, "Context was null.");
            this.currentFile = Objects.requireNonNull(currentFile, "File was null.");
            this.view = Objects.requireNonNull(view, "View was null.");
        }

        @Override
        public void run() {
            Bitmap thumbnail = this.resizeImageForThumbnail(currentFile, ICON_SIZE, ICON_SIZE);
            final Drawable drawable = new BitmapDrawable(this.context.getResources(),
                    Bitmap.createScaledBitmap(thumbnail, ICON_SIZE, ICON_SIZE, true));

            FileAdapter.this.main.runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    DynamicImageGenerator.this.view.setCompoundDrawablesWithIntrinsicBounds(drawable,
                            null, null, null);
                }
            });
        }

        private Bitmap resizeImageForThumbnail(File currentFile, int width, int height) {
            return ThumbnailUtils.extractThumbnail(BitmapFactory.decodeFile(currentFile.getPath()), width, height);
        }
    }

}